###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

# merge with ElectionSentimentCumulative

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# finding the election candidate popularity
SoccerTimeline_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/Chelsea/i) == -1 && doc.text.search(/Arsenal/i) != -1){
		emit(["Arsenal",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[1,doc.geo.coordinates[0],doc.geo.coordinates[1],doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}else if(doc.text.search(/Arsenal/i) == -1 && doc.text.search(/Chelsea/i) != -1){
 		emit(["Chelsea",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[1,doc.geo.coordinates[0],doc.geo.coordinates[1],doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}
}	
}'''

reduce_fun = '''_sum'''

with open('SoccerTimeline.txt','a') as outputfile:
	for row in db.view('_design/final_views002/_view/SoccerTimeline', wrapper=None, group_level = 5, startkey = ["Arsenal",2015,3,26,10], endkey = ["Arsenal",2015,3,26,22]):	
		#print row	
		outputfile.write(str(row.key[4]) + "$?!$" + str(row.key[3]) + "$?!$"  + str(row.key[2]) + "$?!$" + str(row.key[1]) + "$?!$" + str(row.key[0]) + "$?!$"+ str(row.value[6]) + "$?!$"+ str(row.value[0]) + "\n")	
	for row in db.view('_design/final_views002/_view/SoccerTimeline', wrapper=None, group_level = 5, startkey = ["Chelsea",2015,3,26,10], endkey = ["Chelsea",2015,3,26,22]):	
		#print row	
		outputfile.write(str(row.key[4]) + "$?!$" + str(row.key[3]) + "$?!$"  + str(row.key[2]) + "$?!$" + str(row.key[1]) + "$?!$" + str(row.key[0]) + "$?!$"+ str(row.value[6]) + "$?!$"+ str(row.value[0]) + "\n")	
	
