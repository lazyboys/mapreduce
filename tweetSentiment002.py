###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

#server = Server('http://115.146.95.34:5984/')
#db = server['user_timeline']

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# counting the total sentiment value of all tweet
tweetSentiment_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
	 if (doc.sentiment.compound < -0.5){
 		emit("negative",1);
	} else if (doc.sentiment.compound > -0.5 && doc.sentiment.compound < 0.5){
		emit("netural",1);	
	} else if (doc.sentiment.compound > 0.5) {
		emit("positive",1);
	}
}
}'''

reduce_fun = '''function(keys,values,rereduce){
  	return sum(values);
  }'''

with open('tweetSentiment002.txt','a') as outputfile:
	for row in db.view('_design/final_views/_view/tweetSentiment', wrapper=None ,group_level=1):	
		outputfile.write(str(row.key) + "$?!$" + str(row.value) + "\n")
