###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

#server = Server('http://115.146.95.34:5984/')
#db = server['user_timeline']

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# finding count of top tourist attractions in London
TouristAttraction_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	if (doc.text.search(/Buckingham Palace/i) != -1){
		emit("Buckingham Palace",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/London Eye/i) != -1){
		emit("London Eye",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/Harrods/i) != -1){
		emit("Harrods",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/Hyde Park/i) != -1){
		emit("Hyde Park",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/Tower Bridge/i) != -1){
		emit("Tower Bridge",[1,doc.sentiment.compound]);
	} else if(doc.text.search(/Southbank/i) != -1){
		emit("Southbank",[1,doc.sentiment.compound]);
	}
}	
  }'''

reduce_fun = '''_sum'''

with open('TouristAttraction002.txt','a') as outputfile:
	for row in db.view('_design/final_views/_view/TouristAttraction', wrapper=None, group_level = 1):	
		outputfile.write(str(row.key) + "$?!$" + str(row.value[0]) + "$?!$" + str(row.value[1]) +"\n")
