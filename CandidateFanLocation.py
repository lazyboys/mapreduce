###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

#server = Server('http://115.146.95.34:5984/')
#db = server['user_timeline']

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# finding the tweets and coordinates of Arsenal and Chelsea fans
CandidateFanLocation_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	if (doc.text.search(/Ed miliband/i) == -1 && doc.text.search(/David Cameron/i) != -1){
		if (doc.sentiment.compound > 0.75){
	 		emit(["David Cameron",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[doc.text,doc.geo.coordinates,doc.sentiment.compound]);
		}
	} else if(doc.text.search(/David Cameron/i) == -1 && doc.text.search(/Ed miliband/i) != -1){
		if (doc.sentiment.compound > 0.75){
			emit(["Ed miliband",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[doc.text,doc.geo.coordinates,doc.sentiment.compound]);
		}
	}
}
}'''

#reduce_fun = '''_sum'''

with open('CandidateFanLocation.txt','a') as outputfile:
	for row in db.view('_design/final_views/_view/CandidateFanLocation', wrapper=None):
		#print row	
		outputfile.write(str(row.key[0]) + "$?!$" +str(row.value[0].encode('utf-8')) + "$?!$" + str(row.value[1][0])  + "$?!$" + str(row.value[1][1]) + "\n")
