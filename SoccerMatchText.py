###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

# merge with ElectionSentimentCumulative

server = Server('http://144.6.226.10:5984/')
db = server['testdb8']

# finding the election candidate popularity
SoccerMatchText_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/Chelsea/i) == -1 && doc.text.search(/Arsenal/i) != -1){
		emit(["Arsenal",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[doc.text,1,doc.geo.coordinates,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}else if(doc.text.search(/Arsenal/i) == -1 && doc.text.search(/Chelsea/i) != -1){
 		emit(["Chelsea",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[doc.text,1,doc.geo.coordinates,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}
}	
}'''

#reduce_fun = '''_sum'''

with open('SoccerMatchText.txt','a') as outputfile:
	for row in db.query(SoccerMatchText_map_fun,language='javascript'):	
		print row	
		#outputfile.write(str(row.key[1]) + "$?!$"+ str(row.key[2]) + "$?!$" + str(row.key[3]) + "$?!$" + str(row.key[4]) + "$?!$" + str(row.value[0]) + "$?!$" + str(row.value[4]) +"\n")	
	
