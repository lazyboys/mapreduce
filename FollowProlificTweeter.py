###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

#server = Server('http://115.146.95.34:5984/')
#db = server['user_timeline']

#server = Server('http://144.6.226.10:5984/')
#db = server['testdb8']


server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# finding the most prolific tweeter location
FollowProlificTweeter_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
	if (doc.user.id == 2978987457){
 		emit([dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[1,doc.geo.coordinates[0],doc.geo.coordinates[1]]);
	}
}
}'''

with open('FollowProlificTweeter.txt','a') as outputfile:
	for row in db.view('_design/final_views/_view/FollowProlificTweeter', wrapper=None):
		#print row 		
		outputfile.write(str(row.value[1]) + "$?!$" + str(row.value[2]) + "\n")	
	
