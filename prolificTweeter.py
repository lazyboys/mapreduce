###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

#server = Server('http://115.146.95.34:5984/')
#db = server['user_timeline']

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# finding top 10 most prolific tweeter
ProlificTweeter_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	emit(doc.user.id,1);
  }
  }'''

reduce_fun = '''_sum'''

listOfTweeter = []

with open('prolificTweeter.txt','a') as outputfile:
	for i, row in enumerate(db.view('_design/basic_views/_list/sort/user_id', wrapper=None, group_level = 1)):
		if  i <= 10:
			outputfile.write(str(row.value) + "$?!$" + str(row.key) + "\n")
		#listOfTweeter.append(str(row.value) + "$?!$" + str(row.key))
	#listOfTweeter.sort(reverse=True)

	# Display top 10 prolific tweeter
	#listOfTweeter = listOfTweeter[:10]
	#for row in listOfTweeter:
	#	outputfile.write(str(row) + "\n")	
	
