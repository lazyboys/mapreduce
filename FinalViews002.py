###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server
from couchdb import Database

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

#server = Server('http://144.6.226.10:5984/')
#db = server['testdb8']

CandidateTimeline_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/ed miliband/i) == -1 && doc.text.search(/david cameron/i) != -1){
		emit(["David Cameron",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}else if(doc.text.search(/david cameron/i) == -1 && doc.text.search(/ed miliband/i) != -1){
 		emit(["Ed miliband",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}
}	
}'''

SoccerTimeline_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/Chelsea/i) == -1 && doc.text.search(/Arsenal/i) != -1){
		emit(["Arsenal",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[1,doc.geo.coordinates[0],doc.geo.coordinates[1],doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}else if(doc.text.search(/Arsenal/i) == -1 && doc.text.search(/Chelsea/i) != -1){
 		emit(["Chelsea",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[1,doc.geo.coordinates[0],doc.geo.coordinates[1],doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}
}	
}'''


SoccerMatchText_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/Chelsea/i) == -1 && doc.text.search(/Arsenal/i) != -1){
		emit(["Arsenal",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[doc.text,1,doc.geo.coordinates,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}else if(doc.text.search(/Arsenal/i) == -1 && doc.text.search(/Chelsea/i) != -1){
 		emit(["Chelsea",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate(),dt.getUTCHours()],[doc.text,1,doc.geo.coordinates,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}
}	
}'''


sum_reduce_fun = '''_sum'''

design = {  'views': {
			'CandidateTimeline': {
			  'map': CandidateTimeline_map_fun,
			  'reduce': sum_reduce_fun
			},
			'SoccerTimeline': {
			  'map': SoccerTimeline_map_fun,
			  'reduce': sum_reduce_fun
			},
			'SoccerMatchText': {
			  'map': SoccerMatchText_map_fun,
			  #'reduce': sum_reduce_fun
			 }
		 } }


db["_design/final_views002"] = design
