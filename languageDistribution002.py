###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

## merge with sentimentRatingLang

#server = Server('http://115.146.95.34:5984/')
#db = server['user_timeline']


server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# counting the total count of each individual language
languageDistribution_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	emit(doc.lang,[1,doc.sentiment.compound]);
}
}'''

reduce_fun = '''_sum'''

with open('languageDistribution002.txt','a') as outputfile, open('sentimentRatingLang002.txt','a') as outputfile2:
	for row in db.view('_design/final_views/_view/languageDistribution', wrapper=None, group_level = 1):
		outputfile.write(str(row.key) + "$?!$" + str(row.value[0]) + "$?!$" +"\n")
		outputfile2.write(str(row.key) + "$?!$" + str(row.value[1] / row.value[0]) + "\n")		
		#outputfile.write(str(row.key) + "$?!$" + str(row.value[0]) + "$?!$" +str(row.value[1] / row.value[0])  +"\n")

