###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server
from couchdb import Database

server = Server('http://115.146.95.34:5984/')
db = server['alltweet001']



#### for create a doc 
####mappers

CandidatePopularity_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/ed miliband/i) == -1 && doc.text.search(/david cameron/i) != -1){
	if (doc.sentiment.compound > 0.5){
		emit(["David Cameron","David Cameron supporter",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound > -0.5 && doc.sentiment.compound < 0.5){
		emit(["David Cameron","David Cameron swing",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound < -0.5){
		emit(["David Cameron","David Cameron hater",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}
}else if(doc.text.search(/david cameron/i) == -1 && doc.text.search(/ed miliband/i) != -1){
	if(doc.sentiment.compound > 0.5){
		emit(["Ed miliband","Ed miliband supporter",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound > -0.5 && doc.sentiment.compound < 0.5){
 		emit(["Ed miliband","Ed miliband swing",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound < -0.5){
 		emit(["Ed miliband","Ed miliband hater",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}
}
}	
}'''

FollowProlificTweeter_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
	if (doc.user.id == 2978987457){
 		emit(doc.geo.coordinates,1);
	}
}
}'''


languageDistribution_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	emit(doc.lang,[1,doc.sentiment.compound]);
}
}'''




ProlificTweeter_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	emit(doc.user.id,1);
  }
  }'''


SoccerFanLocation_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	if (doc.text.search(/Chelsea/i) == -1 && doc.text.search(/Arsenal/i) != -1){
		if (doc.sentiment.compound > 0.75){
	 		emit(["Arsenal Fan",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[doc.text,doc.geo.coordinates,doc.sentiment.compound]);
		}
	} else if(doc.text.search(/Arsenal/i) == -1 && doc.text.search(/Chelsea/i) != -1){
		if (doc.sentiment.compound > 0.75){
			emit(["Chelsea Fan",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[doc.text,doc.geo.coordinates,doc.sentiment.compound]);
		}
	}
}
}'''

TouristAttraction_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
 	if (doc.text.search(/Buckingham Palace/i) != -1){
		emit("Buckingham Palace",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/London Eye/i) != -1){
		emit("London Eye",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/Harrods/i) != -1){
		emit("Harrods",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/Hyde Park/i) != -1){
		emit("Hyde Park",[1,doc.sentiment.compound]);
	} else if (doc.text.search(/Tower Bridge/i) != -1){
		emit("Tower Bridge",[1,doc.sentiment.compound]);
	} else if(doc.text.search(/Southbank/i) != -1){
		emit("Southbank",[1,doc.sentiment.compound]);
	}
}	
  }'''

tweetSentiment_map_fun = '''function(doc) {
dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
	 if (doc.sentiment.compound < -0.5){
 		emit("negative",1);
	} else if (doc.sentiment.compound > -0.5 && doc.sentiment.compound < 0.5){
		emit("netural",1);	
	} else if (doc.sentiment.compound > 0.5) {
		emit("positive",1);
	}
}
}'''


####reducers

sum_reduce_fun = '''_sum'''


design = {  'views': {
			'CandidatePopularity': {
			  'map': CandidatePopularity_map_fun,
			  'reduce': sum_reduce_fun
			},
			'FollowProlificTweeter': {
			  'map': FollowProlificTweeter_map_fun,
			  #'reduce': sum_reduce_fun
			},
			'languageDistribution': {
			  'map': languageDistribution_map_fun,
			  'reduce': sum_reduce_fun
			},
			'ProlificTweeter': {
			  'map': ProlificTweeter_map_fun,
			  'reduce': sum_reduce_fun
			},
			'SoccerFanLocation': {
			  'map': SoccerFanLocation_map_fun,
			  #'reduce': sum_reduce_fun
			},
			'TouristAttraction': {
			  'map': TouristAttraction_map_fun,
			  'reduce': sum_reduce_fun
			},
			'tweetSentiment': {
			  'map': tweetSentiment_map_fun,
			  'reduce': sum_reduce_fun
			}
		 } }


db["_design/final_views"] = design
