###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

# merge with ElectionSentimentCumulative

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# finding the election candidate popularity
CandidateTimeline_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/ed miliband/i) == -1 && doc.text.search(/david cameron/i) != -1){
		emit(["David Cameron",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}else if(doc.text.search(/david cameron/i) == -1 && doc.text.search(/ed miliband/i) != -1){
 		emit(["Ed miliband",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
}
}	
}'''

reduce_fun = '''_sum'''

with open('CandidateTimeline.txt','a') as outputfile:
	for row in db.view('_design/final_views002/_view/CandidateTimeline', wrapper=None, group_level = 4):	
		#print row	
		outputfile.write(str(row.key[3]) + str(row.key[2]) + str(row.key[1]) + "$?!$" + str(row.key[0]) + "$?!$" + str(row.value[4]) +"\n")	
	
 
