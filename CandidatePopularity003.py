###############################
#Team:	3
#City:	London
#Name:
#	Lu Fan (646627)
#	Thanh Quang Minh Pham (633262)
#	Renji Luke Harold (671917)
#	Kian Boon Mark Lee (665893)
#	Md Akmal Hossain (662254)
#
###############################

from couchdb import Server

# merge with ElectionSentimentCumulative

server = Server('http://115.146.95.34:5984/')
db = server['user_april_may']

# finding the election candidate popularity
CandidatePopularity_map_fun = '''function(doc) {

dt = new Date(doc.created_at);
if(dt.getUTCFullYear() == 2015 & dt.getUTCMonth() >= 3){
if(doc.text.search(/ed miliband/i) == -1 && doc.text.search(/david cameron/i) != -1){
	if (doc.sentiment.compound > 0.5){
		emit(["David Cameron","David Cameron supporter",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound > -0.5 && doc.sentiment.compound < 0.5){
		emit(["David Cameron","David Cameron swing",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound < -0.5){
		emit(["David Cameron","David Cameron hater",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}
}else if(doc.text.search(/david cameron/i) == -1 && doc.text.search(/ed miliband/i) != -1){
	if(doc.sentiment.compound > 0.5){
		emit(["Ed miliband","Ed miliband supporter",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound > -0.5 && doc.sentiment.compound < 0.5){
 		emit(["Ed miliband","Ed miliband swing",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}else if(doc.sentiment.compound < -0.5){
 		emit(["Ed miliband","Ed miliband hater",dt.getUTCFullYear(),dt.getUTCMonth(),dt.getUTCDate()],[1,doc.sentiment.pos,doc.sentiment.neu,doc.sentiment.neg,doc.sentiment.compound]);
	}
}
}	
}'''

reduce_fun = '''_sum'''

with open('CandidatePopularity003.txt','a') as outputfile:
	for row in db.view('_design/final_views/_view/CandidatePopularity', wrapper=None, group_level = 2):	
		#print row	
		outputfile.write(str(row.key[1]) + "$?!$" + str(row.value[0])+ "\n")	
	
with open('ElectionSentimentCumulative003.txt','a') as outputfile2:
	for row in db.view('_design/final_views/_view/CandidatePopularity', wrapper=None, group_level=1):	
		outputfile2.write(str(row.key[0]) + "$?!$" + str(row.value[1]) + "$?!$" + str(row.value[2]) + "$?!$" + str(row.value[3]) + "$?!$" + str(row.value[4]) + "\n")

# with open('UKElectionSentiment003.txt','a') as outputfile3:
# 	for row in db.query(CandidatePopularity_map_fun,reduce_fun,language='javascript',group_level = 3):	
# 		#print row
# 		outputfile3.write(str(row.key[0]) + "$?!$" + str(row.key[1]) + "$?!$" + str(row.value[0]) + "\n")
